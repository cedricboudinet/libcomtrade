#include "comtrade.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <fstream>

char * strsplit(char * start, char delim, char ** end)
{
	char *pCh;
	pCh=start;
	*end=pCh+1;
	while(*pCh!='\0')
	{
		if(*pCh==delim)
		{
			*pCh='\0';
			*end=pCh+1;
			break;
		}
		pCh+=1;
	}
	return start;
}

//strcnt counts number of occurences of chr in str
int strcnt(const char * str, char chr)
{
	int cnt = 0;
	const char *pChr;
	pChr=str;
	while(*pChr!='\0')
	{
		if(*pChr==chr) cnt++;
		pChr+=1;
	}
	return cnt;
}

std::vector<std::string> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

//getline function with automatic line and fields count
int comtrade_getline(std::vector<std::string> & lineSplit, std::ifstream & stream, int * p_line_cnt)
{
    std::string line;
	std::getline(stream, line);
	lineSplit = split(line, ",");
	(*p_line_cnt)+=1;
	return lineSplit.size();
}

comtrade_status_t comtrade_parseConfig(comtrade_t * comtrade, std::ifstream & fp_comtradeCfg)
{
	int *iLine;
	iLine = &(comtrade->error_line);
	*iLine=0;
	std::vector<std::string> lineSplit;
	int nbElem;

	//1st line
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	if(nbElem!=2) return COMTRADE_CFG_OPEN_ERROR;
	comtrade->station_name = lineSplit[0]; //station name
	comtrade->station_id = strtol(lineSplit[1].c_str(), NULL, 10); //station id

	//2nd line
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	if(nbElem!=3) return COMTRADE_CFG_PARSING_ERROR;
	comtrade->nbTotChannels=strtol(lineSplit[0].c_str(), NULL, 10);
	comtrade->nbAnalogChannels=strtol(lineSplit[1].c_str(), NULL, 10);
	comtrade->nbDigitalChannels=strtol(lineSplit[2].c_str(), NULL, 10);

	//TODO init structure
	comtrade->analogChannels.resize(comtrade->nbAnalogChannels);

	//AnalogChannels config lines
	for(size_t iCh=0;iCh<comtrade->nbAnalogChannels;iCh++)
	{
		nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
		if(nbElem!=10) return COMTRADE_CFG_PARSING_ERROR;
		size_t chNumber = strtol(lineSplit[0].c_str(), NULL, 10);
		if(chNumber!=(iCh+1)) return COMTRADE_CFG_PARSING_ERROR;
		comtrade->analogChannels[iCh].name = lineSplit[1]; //channel name
		comtrade->analogChannels[iCh].phase = lineSplit[2]; //channel phase
		comtrade->analogChannels[iCh].circuit = lineSplit[3]; //channel circuit
		comtrade->analogChannels[iCh].unit = lineSplit[4]; //channel unit

		
		comtrade->analogChannels[iCh].a = strtod(lineSplit[5].c_str(), NULL); //channel a
		comtrade->analogChannels[iCh].b = strtod(lineSplit[6].c_str(), NULL); //channel b
		comtrade->analogChannels[iCh].skew = strtod(lineSplit[7].c_str(), NULL); //channel skew
		comtrade->analogChannels[iCh].mini = strtol(lineSplit[8].c_str(), NULL, 10); //channel min
		comtrade->analogChannels[iCh].maxi = strtol(lineSplit[9].c_str(), NULL, 10); //channel max

	}
	//TODO init structure
	comtrade->digitalChannels.resize(comtrade->nbDigitalChannels);
	for(size_t iCh=0;iCh<comtrade->nbDigitalChannels;iCh++)
	{
		nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
		if(nbElem!=3) return COMTRADE_CFG_PARSING_ERROR;
		size_t chNumber = strtol(lineSplit[0].c_str(), NULL, 10);
		if(chNumber!=(iCh+1)) return COMTRADE_CFG_PARSING_ERROR;
		comtrade->digitalChannels[iCh].name = lineSplit[1]; //ch. name
		comtrade->digitalChannels[iCh].m = strtol(lineSplit[2].c_str(), NULL, 10); //ch. state
		comtrade->digitalChannels[iCh].circuit = std::string("");
		comtrade->digitalChannels[iCh].phase = std::string("");
		comtrade->digitalChannels[iCh].unit = std::string("");
	}
	//line frequency
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	if(nbElem!=1) return COMTRADE_CFG_PARSING_ERROR;
	comtrade->line_frequency = strtod(lineSplit[0].c_str(), NULL);

	//nrates
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	if(nbElem!=1) return COMTRADE_CFG_PARSING_ERROR;
	comtrade->nrates= strtod(lineSplit[0].c_str(), NULL); //lf

	comtrade->samplerates.resize(comtrade->nrates);
	for(int isr=0;isr<comtrade->nrates;isr++)
	{
		nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
		if(nbElem!=2) return COMTRADE_CFG_PARSING_ERROR;
		comtrade->samplerates[isr].samp = strtod(lineSplit[0].c_str(), NULL);
		comtrade->samplerates[isr].endsamp = strtol(lineSplit[1].c_str(), NULL, 10);
	}

	//timestamps
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	comtrade->startTimestamp=lineSplit[0]+","+lineSplit[1];
	comtrade->startTimestamp+='\0';
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	comtrade->triggerTimestamp=lineSplit[0]+","+lineSplit[1];
	comtrade->triggerTimestamp+='\0';

	//ft
	nbElem = comtrade_getline(lineSplit, fp_comtradeCfg, iLine);
	comtrade->filetype=lineSplit[0];
	comtrade->filetype+='\0';
	//free(line);
	return COMTRADE_OK;
}

comtrade_status_t comtrade_parse(comtrade_t * comtrade, const char * comtradeConfigFile, const char * comtradeDataFile)
{

	//Config file parsing
	//FILE * fp_comtradeCfg = fopen(comtradeConfigFile ,"r");
	std::ifstream fp_comtradeCfg(comtradeConfigFile);
	if(!fp_comtradeCfg.is_open())
		return COMTRADE_CFG_OPEN_ERROR;
	int iLine=0;
	int nbElem;
	std::vector<std::string> lineSplit;

	comtrade_status_t st = comtrade_parseConfig(comtrade, fp_comtradeCfg);
	fp_comtradeCfg.close();

	if(st!=COMTRADE_OK) return st;

	//Data file parsing
	std::ifstream fp_comtradeDat(comtradeDataFile);
	if(!fp_comtradeDat.is_open())
		return COMTRADE_DAT_OPEN_ERROR;
	iLine=0;
	if(comtrade->nrates>1)
	{
		fp_comtradeDat.close();
		return COMTRADE_NOT_IMPLEMENTED;
	}
	comtrade_alloc_data(comtrade);
	for(int irate = 0; irate < comtrade->nrates; irate++)
	{
		for (int isamp=0; isamp<comtrade->samplerates[irate].endsamp;isamp++)
		{
			nbElem = comtrade_getline(lineSplit, fp_comtradeDat, &iLine);
			if(((size_t)nbElem)!=(comtrade->nbTotChannels+2))
				goto dataParsingError;
			comtrade->data.sampleNumbers[isamp]=strtol(lineSplit[0].c_str(), NULL, 10);
			comtrade->data.time_us[isamp]=strtol(lineSplit[1].c_str(), NULL, 10);
			comtrade->data.time_s_real[isamp]=1e-6 * comtrade->data.time_us[isamp];
			for(size_t iCh=0;iCh<comtrade->nbAnalogChannels;iCh++)
			{
				//TODO: manage missing values
				int analogData=strtol(lineSplit[2+iCh].c_str(), NULL, 10);
				comtrade->data.analogDataAsInt[iCh][isamp]=analogData;
				comtrade->data.analogDataAsReal[iCh][isamp]=
					(analogData*comtrade->analogChannels[iCh].a)+comtrade->analogChannels[iCh].b;
			}
			for(size_t iCh=0;iCh<comtrade->nbDigitalChannels;iCh++)
			{
			//	//TODO: manage missing values
				comtrade->data.digitalData[iCh][isamp]=strtol(lineSplit[2+iCh+comtrade->nbAnalogChannels].c_str(), NULL, 10);
			}
		}
	}

	fp_comtradeDat.close();
	return COMTRADE_OK;

dataParsingError:
	comtrade->error_line=iLine;
	printf("Error at line %d\n", iLine);
	fp_comtradeCfg.close();
	return COMTRADE_DAT_PARSING_ERROR;
}

void comtrade_init(comtrade_t * ctx)
{
	ctx->data.nbSamples=0;
}

void comtrade_destroy(comtrade_t * ctx)
{
	delete ctx;
}

void comtrade_alloc_data(comtrade_t * ctx)
{
	int nbSamples=ctx->samplerates[0].endsamp;
	ctx->data.nbSamples=nbSamples;
	ctx->data.sampleNumbers.resize(nbSamples);
	ctx->data.time_us.resize(nbSamples);
	ctx->data.time_s_real.resize(nbSamples);
	ctx->data.analogDataAsInt.resize(ctx->nbAnalogChannels);
	for(size_t iCh=0;iCh<ctx->nbAnalogChannels;iCh++)
	{
		ctx->data.analogDataAsInt[iCh].resize(nbSamples);
	}


	ctx->data.digitalData.resize(ctx->nbDigitalChannels);
	for(size_t iCh=0;iCh<ctx->nbDigitalChannels;iCh++)
	{
		ctx->data.digitalData[iCh].resize(nbSamples);
	}
	ctx->data.analogDataAsReal.resize(ctx->nbAnalogChannels);
	for(size_t iCh=0;iCh<ctx->nbAnalogChannels;iCh++)
	{
		ctx->data.analogDataAsReal[iCh].resize(nbSamples);
	}

}

comtrade_t * comtrade_new()
{
	comtrade_t * comtrade = new comtrade_t();
	return comtrade;
}

double * comtrade_get_analogChannelDataAsReal(comtrade_t * ctx, int iCh)
{
	return ctx->data.analogDataAsReal[iCh].data();
}

int comtrade_get_index_for_analog_channel(const comtrade_t *comtrade, const char * name)
{
	for(auto it=comtrade->analogChannels.begin(); it!=comtrade->analogChannels.end(); it++)
	{
		if(it->name==name)
			return (it-comtrade->analogChannels.begin());
	}
	return -1;
}

int32_t comtrade_get_end_time_us(const comtrade_t * comtrade)
{
	return comtrade->data.time_us.back();
}

int32_t comtrade_get_nb_samples(const comtrade_t * comtrade)
{
	return comtrade->data.time_us.size();
}
const double * comtrade_get_sampletimes_as_real(const comtrade_t * comtrade)
{
	return comtrade->data.time_s_real.data();
}
