import math
dat=open('bigsinus.dat','w')

for i in range(50000):
    ts=i/4800.
    VA=30000*math.sin(2*math.pi*50*ts)
    VB=30000*math.sin(2*math.pi*50*ts-2*math.pi/3)
    VC=30000*math.sin(2*math.pi*50*ts+2*math.pi/3)
    IA=1000*math.sin(2*math.pi*50*ts)
    IB=1000*math.sin(2*math.pi*50*ts-2*math.pi/3)
    IC=1000*math.sin(2*math.pi*50*ts+2*math.pi/3)
    line="%d,%d,%d,%d,%d,%d,%d,%d"%(i+1,ts*1e6,VA,VB,VC,IA,IB,IC)
    dat.write(line+'\n')
