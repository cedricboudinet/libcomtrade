#ifndef LIBCOMTRADE_H
#define LIBCOMTRADE_H
#include <stdint.h>
#include <stdio.h>
//! comtrade status

typedef enum {
	COMTRADE_OK=0,
	COMTRADE_CFG_OPEN_ERROR=1,
	COMTRADE_DAT_OPEN_ERROR=2,
	COMTRADE_CFG_PARSING_ERROR=3,
	COMTRADE_DAT_PARSING_ERROR=4,
	COMTRADE_NOT_IMPLEMENTED=255

} comtrade_status_t;
#ifdef __cplusplus
#include <vector>
#include <string>
//! comtrade channel
typedef struct {
	std::string circuit;
	std::string phase;
	std::string name;
	std::string unit;
	double a,b, skew;
	int mini, maxi;
	int m; //for digital channels
} comtrade_channel_t;

typedef struct {
	double samp; //sample rate
	int endsamp;

} comtrade_samplerate_t;

typedef struct {
	int nbSamples;
	std::vector<uint32_t> sampleNumbers;
	std::vector<uint32_t> time_us;
	std::vector< std::vector<int> > analogDataAsInt;
	std::vector< std::vector<double> > analogDataAsReal;
	std::vector< std::vector<int> > digitalData;
	std::vector<double> time_s_real;
} comtrade_data_t;

struct _comtrade_t {
	std::string station_name;
	int station_id;
	size_t nbTotChannels;
	size_t nbAnalogChannels;
	size_t nbDigitalChannels;
	std::vector<comtrade_channel_t> digitalChannels;
	std::vector<comtrade_channel_t> analogChannels;
	double line_frequency;
	int nrates;
	std::vector<comtrade_samplerate_t> samplerates;
	std::string startTimestamp;
	std::string triggerTimestamp;
	std::string filetype;
	int error_line;
	comtrade_data_t data;
} ;
int comtrade_getline(std::vector<std::string> & lineSplit, std::ifstream & stream, int * p_line_cnt);
extern "C" {
#else
//typedef struct _comtrade_t comtrade_t;
#endif
typedef struct _comtrade_t comtrade_t;
/**
	\brief
	Initiates a comtrade instance
	@param comtrade comtrade_t instance to initialize
*/
void comtrade_init(comtrade_t * comtrade);
/**
	\brief
	Creates a new comtrade instance
	@returns new comtrade instance
*/
comtrade_t * comtrade_new();
/**
	\brief{Destroys comtrade}
	Destroys comtrade instance
	@param ctx comtrade instance to destroy
*/
void comtrade_destroy(comtrade_t * ctx);
/**
	\brief
	Parses comtrade files
	@param comtrade instance
	@param comtradeConfigFile path to config file
	@param comtradeDataFile path to data file
	@returns status of parsing
*/
comtrade_status_t comtrade_parse(comtrade_t * comtrade, const char * comtradeConfigFile, const char * comtradeDataFile);
//! returns C double array for a given analog channel
/**
	\brief
	get C double array for an analog channel
	@param comtrade instance
	@param iChannel channel index
	@returns pointer to C double array
*/
double * comtrade_get_analogChannelDataAsReal(comtrade_t *, int);
void comtrade_alloc_data(comtrade_t *);
//
char * strsplit(char * start, char delim, char ** end);
int strcnt(const char * str, char chr);
/**
	\brief
	get index of an analog channel
	@param comtrade comtrade instance
	@param name analog channel name
	@returns index of analog channel, -1 if the channel is not found
*/
int comtrade_get_index_for_analog_channel(const comtrade_t *comtrade, const char * name);
/**
	\brief
	get end time of a comtrade
	@param comtrade comtrade instance
	@returns end time in microseconds
*/
int32_t comtrade_get_end_time_us(const comtrade_t * comtrade);

/**
	\brief
	get number of samples in a comtrade
	@param comtrade comtrade instance
	@returns number of samples
*/
int32_t comtrade_get_nb_samples(const comtrade_t * comtrade);

const double * comtrade_get_sampletimes_as_real(const comtrade_t *);
#ifdef __cplusplus
}
#endif

#endif
