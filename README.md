# libcomtrade



## Overview

libcomtrade is an open-source COMTRADE (Common format for Transient Data Exchange for power systems) parsing library.
It is implemented in C++ and a C binding is available.

The library can only parse IEEE C37.111-1991 .cfg and .dat files.

## Prerequisites

- CMake
- Cpputest

## Building

CMake is used as build system. To build simply run:
```
cmake .
make
```

## Testing
Unit testing are implemented with Cpputest and managed by CMake. To run unit tests, run:

```
make test
```

A valgrind script allows to check memory usage and is available in tests/run\_valgrind.sh

## Packaging
CMake is also configured as a packaging system. For example to build a debian package, run:
```
cpack -G DEB
```

## TODO
 - Parsing 1999 and 2013 files
 - Parsing binary files
