#include <comtrade.h>
#include "CppUTest/CommandLineTestRunner.h"
#include <fstream>
TEST_GROUP(FirstTestGroup)
{
};

TEST(FirstTestGroup, StrSplit)
{
	char ligne[] = "e,rr,,o,r";
	char *nextSubStr=NULL;
	nextSubStr=ligne;
	STRCMP_EQUAL("e", strsplit(nextSubStr,',',&nextSubStr));
	STRCMP_EQUAL("rr", strsplit(nextSubStr,',',&nextSubStr));
	STRCMP_EQUAL("", strsplit(nextSubStr,',',&nextSubStr));
	STRCMP_EQUAL("o", strsplit(nextSubStr,',',&nextSubStr));
	STRCMP_EQUAL("r", strsplit(nextSubStr,',',&nextSubStr));
	STRCMP_EQUAL("\0", strsplit(nextSubStr,',',&nextSubStr));
}

TEST(FirstTestGroup, StrCnt)
{
	char line[] = "e,rr,,o,r";
	CHECK_EQUAL(4, strcnt(line, ','));
	CHECK_EQUAL(3, strcnt(line, 'r'));
}

TEST(FirstTestGroup, Comtrade_Getline)
{
	//FILE * fp_comtradeDat = fopen(COMTRADE_FILES_PATH"/sample1.cfg","r");
	std::ifstream fp_comtradeDat(COMTRADE_FILES_PATH"/sample1.cfg");
	//char * line=NULL;
	int iLine=0;
	int nbElem;
	std::vector<std::string> lineSplit;
	nbElem = comtrade_getline(lineSplit, fp_comtradeDat, &iLine);
	CHECK_EQUAL(2, nbElem);
	nbElem = comtrade_getline(lineSplit, fp_comtradeDat, &iLine);
	CHECK_EQUAL(3, nbElem);
	CHECK_EQUAL(2, iLine);
	
}

TEST(FirstTestGroup, SecondTest)
{
	comtrade_t comtrade;
	//COMTRADE_FILES_PATH;
	comtrade_init(&comtrade);
	//POINTERS_EQUAL(NULL, comtrade.analogChannels);

	CHECK_EQUAL(COMTRADE_CFG_OPEN_ERROR, comtrade_parse(&comtrade, COMTRADE_FILES_PATH"/doesntexist.cfg", COMTRADE_FILES_PATH"/sample1.dat"));
	CHECK_EQUAL(COMTRADE_DAT_OPEN_ERROR, comtrade_parse(&comtrade, COMTRADE_FILES_PATH"/sample1.cfg", COMTRADE_FILES_PATH"/doesntexist.dat"));
	CHECK_EQUAL(COMTRADE_OK, comtrade_parse(&comtrade, COMTRADE_FILES_PATH"/sample1.cfg", COMTRADE_FILES_PATH"/sample1.dat"));
	STRCMP_EQUAL("testStation", comtrade.station_name.c_str());
	CHECK_EQUAL(42, comtrade.station_id);
	CHECK_EQUAL(3, comtrade.nbTotChannels);
	CHECK_EQUAL(2, comtrade.nbAnalogChannels);
	CHECK_EQUAL(1, comtrade.nbDigitalChannels);
	CHECK_EQUAL(0, comtrade_get_index_for_analog_channel(&comtrade, "VA"));
	CHECK_EQUAL(1, comtrade_get_index_for_analog_channel(&comtrade, "VB"));
	CHECK_EQUAL(-1, comtrade_get_index_for_analog_channel(&comtrade, "AVA"));
	STRCMP_EQUAL("VA", comtrade.analogChannels[0].name.c_str());
	STRCMP_EQUAL("VB", comtrade.analogChannels[1].name.c_str());
	STRCMP_EQUAL("A", comtrade.analogChannels[0].phase.c_str());
	STRCMP_EQUAL("",  comtrade.analogChannels[1].phase.c_str());
	STRCMP_EQUAL("", comtrade.analogChannels[0].circuit.c_str());
	STRCMP_EQUAL("circuit", comtrade.analogChannels[1].circuit.c_str());
	STRCMP_EQUAL("kV", comtrade.analogChannels[0].unit.c_str());
	STRCMP_EQUAL("V",  comtrade.analogChannels[1].unit.c_str());
	DOUBLES_EQUAL(.05, comtrade.analogChannels[0].a, 1e-5);
	DOUBLES_EQUAL(.01, comtrade.analogChannels[1].a, 1e-5);
	DOUBLES_EQUAL(.1, comtrade.analogChannels[0].b, 1e-5);
	DOUBLES_EQUAL(.2, comtrade.analogChannels[1].b, 1e-5);
	DOUBLES_EQUAL(1, comtrade.analogChannels[0].skew, 1e-5);
	DOUBLES_EQUAL(2.5, comtrade.analogChannels[1].skew, 1e-5);
	CHECK_EQUAL(-30000, comtrade.analogChannels[0].mini);
	CHECK_EQUAL(-20000, comtrade.analogChannels[1].mini);
	CHECK_EQUAL(30000, comtrade.analogChannels[0].maxi);
	CHECK_EQUAL(20000, comtrade.analogChannels[1].maxi);

	STRCMP_EQUAL("D0",  comtrade.digitalChannels[0].name.c_str());
	CHECK_EQUAL(0,  comtrade.digitalChannels[0].m);

	DOUBLES_EQUAL(50., comtrade.line_frequency, 1e-5);

	CHECK_EQUAL(1, comtrade.nrates);
	DOUBLES_EQUAL(5000, comtrade.samplerates[0].samp, 1e-3);
	CHECK_EQUAL(10, comtrade.samplerates[0].endsamp);

	STRCMP_EQUAL("06/29/17,11:24:57.888", comtrade.startTimestamp.c_str());
	STRCMP_EQUAL("06/29/17,11:24:58.888", comtrade.triggerTimestamp.c_str());
	STRCMP_EQUAL("ASCII", comtrade.filetype.c_str());

	CHECK_EQUAL(1, comtrade.data.sampleNumbers[0]);
	CHECK_EQUAL(2, comtrade.data.sampleNumbers[1]);
	CHECK_EQUAL(0, comtrade.data.time_us[0]);
	CHECK_EQUAL(200, comtrade.data.time_us[1]);
	CHECK_EQUAL(1800, comtrade_get_end_time_us(&comtrade));
	for(int i=0;i<10;i++)
	{
		CHECK_EQUAL((i+1)*2, comtrade.data.analogDataAsInt[0][i]);
		DOUBLES_EQUAL(((i+1)*2)*.05+.1, comtrade.data.analogDataAsReal[0][i], 1e-3);
		DOUBLES_EQUAL(comtrade.data.analogDataAsReal[0][i], comtrade_get_analogChannelDataAsReal(&comtrade, 0)[i],1e-3);
		CHECK_EQUAL((i+1)*2+1, comtrade.data.analogDataAsInt[1][i]);
		CHECK_EQUAL(((i+1)*2+1)*.01+.2, comtrade.data.analogDataAsReal[1][i]);
		CHECK_EQUAL(i%2, comtrade.data.digitalData[0][i]);
		CHECK_EQUAL(1e-6 * (i*200), comtrade_get_sampletimes_as_real(&comtrade)[i]);
	}

	CHECK_EQUAL(10, comtrade_get_nb_samples(&comtrade));

	//checking error line number when parsing failed
	CHECK_EQUAL(COMTRADE_CFG_PARSING_ERROR, comtrade_parse(&comtrade, COMTRADE_FILES_PATH"/bad.cfg", COMTRADE_FILES_PATH"/sample1.dat"));
	CHECK_EQUAL(4, comtrade.error_line);
}

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}
